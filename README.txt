README.txt

Title: "Maternal Education and Maternal Mortality: Evidence from a Large Panel 
				and Various Natural Experiments"
Authors: Sonia Bhalotra, Damian Clarke
Contact: S.Bhalotra@bristol.ac.uk, damian.clarke@economics.ox.ac.uk

This folder contains the cross-country data used in the above paper. We provide
the full raw data accessed from World Bank, Demographic and Health Surveys and 
World Health Organization in csv and dta format.  These are:
	> AttendedBirths
	> BL2010_F_v1.2 (Barro-Lee education data)
	> Fertility
	> GDPpc
	> Immunization
	> MMR
	> Population
	> TeenBirths

We also provide the merged data file with all variables used for regression and
descriptive results.  This file is:
	> BhalotraClarke2013
